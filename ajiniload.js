/** 
 * Создание структур данных устройств для их автоматического опроса
 * @author GerasimovGerasim <dialix@yandex.ru>
 * @version 0.1
*/


//исползую jsdoc comments
//https://webformyself.com/dokumentirovanie-proektov-javascript/

//загрузка ini файла
module.exports.getDevices = getDevices;
var fs = require('fs');
var iconv = require('iconv-lite');//использую для перекодировки cp1251 в utf8
var path = require('path');
///////////////////////////
var IniFile = require('./lib/inifile/inifile');
var Aj = require('./ajobjects');

/*
//devList.setVarFromIniStr('IrScale=0,1640625');
devList.setVarFromIniStr('IrScale=x00#300A|75mV# 0,0984375/x01#500A|75mV# 0,1640625/x02#750A|75mV# 0,24609375/x01/');
devList.setValueFromIniStr('p00700=IzMnMax/Макс.задание ручной регулировки тока/TWORD/xххх/r2007/A/IrScale%2,4/2/Iz/1,4/x06AB/','RAM');
devList.setValueFromIniStr('p00800=IzMnMin/Мин.задание ручной регулировки тока/TWORD/xххх/r2008/A/IrScale*2,4/2/Iz/0,1/x0123/', 'RAM');
devList.setValueFromIniStr('p03400=Iomin/Ограничение тока при перегрузе/TWORD/xF044/r2022/A/IrScale/2/Iz/1,05/x0500/','FLASH');
devList.setValueFromIniStr('p03300=Iomax/Ограничение тока при отсутствии перегруза/TWORD/xF042/r2021/A/IrScale/2/Iz/1,8/x0892/','CD');
//devList.createRequest();
*/
var Devices = [];

//getDevices()
//Заполняет переменную Devices структурами файлов устройств с настройками связи
    /*что-то вроде этого в 
        Devises {
            dev0 {
                files:['config.json','DExSSMFCB.ini'],
                path:'./devices/dev0'
                config:'строка содержимого JSON-файла конфигурации (связь и прочее)'
                devFileName:'строка содержащая имя файла'
                deviceContent:'строка содержащая весь ini-файл'
                                (оно точно надо? может кусками подгружать?)
                                с другой стороны раз подгрузив работаешь потом только с оперативкой
                                давай не контент подгружу, а имя файла сформирую потом напишу TIniFile и буду уже контент из файла грузить
                DevData[] //массив данных для каждой секции параметров (vars, ram, cd, flash)
                          //iniArray - массив строк секции из ini-файла
                    vars [iniArray],
                    FLASH [iniArray],
                    RAM[iniArray],
                    CD[iniArray]

            },
            dev1 {
                files:[],
                path:'./devices/dev1'
            }
        }
    */

/**
 * @description Cобирает начальную информацию из директории "Устройства"
 * @param {String} dirName - Путь к директории #devices где содержаться вложенные директории
 *                           типа dev, с файлами конфигурации (типа config.json и dexssmfcb.ini)
 * @returns {Array} Devices - возвращает масив вида:
 *         Devises {
            dev0 {
                files:['config.json','DExSSMFCB.ini'],
                path:'./devices/dev0'
                config:'строка содержимого JSON-файла конфигурации (связь и прочее)'
                devFileName:'строка содержащая имя файла'
                deviceContent:'строка содержащая весь ini-файл'
                                (оно точно надо? может кусками подгружать?)
                                с другой стороны раз подгрузив работаешь потом только с оперативкой
                                давай не контент подгружу, а имя файла сформирую потом напишу TIniFile и буду уже контент из файла грузить
                DevData[] //массив данных для каждой секции параметров (vars, ram, cd, flash)
                          //iniArray - массив строк секции из ini-файла
                    vars [iniArray],
                    FLASH [iniArray],
                    RAM[iniArray],
                    CD[iniArray]

            },
            dev1 {
                files:[],
                path:'./devices/dev1'
            }
        }
 */
function getDevsFiles(dirName){
    let res = [];
    //1) Просканировать директорию devices, составить список вложенных в неё директорий
    let DirList = fs.readdirSync(dirName,"utf8");
    console.log('getDevsFiles.DirList:',DirList);
    //2) теперь пройтись по директориям devх в поисках файлов с расширением ini
    let i = DirList.length;
    while (i-- !=0) {
        let o = {};// res[DirList[i]] = {};
        o.name = DirList[i];
        o.path = dirName+"/"+DirList[i]+'/';
        o.files = fs.readdirSync(o.path,"utf8");
        res.push(o);
    }
    console.log('getDevsFiles.Devices:',res);
    return res;
}

function getMemTypesNamesFromConfig(config){
    console.log(config);
    var res = [];
    //надо трансформировать его в объект
    //вытянуть секцию tags
    if (config != undefined) {
        var o = JSON.parse(config);
        console.log(o.tags);
        for (key in o.tags){
            res.push(key);
        }
        return res;
    }
    return null;//нечего возвращать
}

//getDevicesTags
//Для каждого Устройства создаёт массив DevData
//Там создаёт ключи для полей имеющихся в ini:
//  vars (если есть, так как не указывается в config, но без него нельзя)
//  RAM, CD, FLASH если указаны в config
//Для каждого ключа, загружает массивы строк из соответствующей секции ini-файла 
function getDevicesTagsFromIni(devs){
    var i = devs.length;
    while (i-- !=0){
        var MemTypes = getMemTypesNamesFromConfig(devs[i].config);
        if (IniFile.isSection('vars',devs[i].devFileName)) {//
            MemTypes.splice(0,0,'vars');
        }
        //получил какие массив с названиями секций ini которые востребованы (RAM, FLASH, CD)
        if (MemTypes != null) {
            //Добавлю DevData свойство в devs[i]
            devs[i].DevData = [];
            //создать объект ПараметрыУстройства
            //зная ini и то что хоть какие-то секции есть, загрузить секцию vars (ну если она есть)
            //ПараметрыУстройства.setVarFromIniStr
           MemTypes.forEach(function(item){
                //теперь зная имя Ini-файла и название требуемых секций, надо окрыть ini, и построчно (ну или списком)
                //читать содержимое секций, выделять из них сроки и с помощью ПараметрыУстройства.setValueFromIniStr
                var a = IniFile.getSectionListFromIniFile(item,devs[i].devFileName);
                devs[i].DevData.push(item);//создаю ключ с именем как в строке item
                devs[i].DevData[item] = a; //и добавляю в него созданный массив
                //в массиве строки с параметрами, их направить на парсинг7
           });
        }
    }
}

/** @description Создание Ajuster объектов из извлечённых ранее строк ini-файла для разных секций (vars, RAM, CD, FLASH),
 * 
 * 
 * @param {Array} devs - массив устройств циклически опрашиваемых системой
 */
function getDeviceDataFromTags(devs){
    devs.forEach (function (value, index) {
        if (value.hasOwnProperty('DevData')){//ini был прочитан и массивы строк создыны
            //1. Создать Ajuster - объект, и добавить его к свойствам
            value.ajuster = new Aj.TDeviceList();
            //2. Загрузить "vars"-секцию, она должна быть первой (в случае если она есть)
            if ('vars' in value.DevData){
                console.log(value.DevData['vars']);
                value.ajuster.setVarsFromList(value.DevData['vars']);
                console.log(value);
            }
            //3. Загрузить остальные секции (если попадётся vars - пропустить)
            value.DevData.forEach(function(key,index, arr){ 
                if (key != 'vars') {
                    value.ajuster.setValuesFromList(arr[key], key);//получаю список объектов
                    value.ajuster.createRequest(key);//формирую оптимизированные параметры запроса данных для этой секции
                }
            });
        }
    });
}

function getDevices(){
    Devices = getDevsFiles("./devices");//просканировать директорию devices, взять оттуда начальную информацию
    //на этом этапе у меня появляются
    //Devices[1].files[0] = "config.json"
    //Devices[1].files[1] = "DExSSMFCB1.ini"
    //Devices[1].name = "dev0"
    //Devices[1].path = "./devices/dev0/"
    //Заполняет config (настройки связи) и deviceContent (содержимое ini-файла параметров устройства)
    loadDevXContent(Devices);
    //После loadDevXContent у меня дерево Devices заполнилось:
    //Devices[1].config - имеет содержимое файла config.json
    //Devices[1].devFileName ="./devices/dev0/DExSSMFCB1.ini - название ini-файла с полным путём для него
    //Теперь из Ini-файлов вытащить данные устройства, для тех секция что прописаны в config в секции tags (это: RAM, FLASH, CD делается UPercase)
    getDevicesTagsFromIni(Devices);//вытящил все данные из ini-файлов
    console.log('getDevicesTagsFromIni: ',Devices);
    getDeviceDataFromTags(Devices);//на основании полученных списков строк из ini-файлов,
                                   //создать Ajuster объекты
                                   //и их поля req в которых указаны параметры регистров
                                   //для формирования команд чтения и записи
    console.log('getDeviceDataFromTags: ',Devices);
}
    //loadCongigFile загрузка config файла с настройками связи
    function loadConfigFile(filename, o){
        var text = fs.readFileSync(filename, "utf8");//загрузил config  
        o.config = text;
    }

    //в объект добавляет поле с полным именем файла данных ini
    function getIniFullName(filename,o) {
        o.devFileName = filename;
    }

    //loadFiles перебирает файлы и загружает их контент
    function loadFiles(o){
        let i = o.files.length;//получаю кол-во файлов в списке
        while (i-- !=0) {//теперь поочереди их читаю
            let filename = o.path + o.files[i];
            let ext = path.extname(o.files[i]);
            switch (ext) {
                case '.ini': //loadIniFile(filename, o);
                            getIniFullName(filename,o);
                            break;
                case '.json':loadConfigFile(filename, o); 
                            break;
            }
        }

    }

    //loadDevXContent загрузка контента из файлов содержащихся в Devices/DevX - директориях
    //Заполняет config (настройки связи) и deviceContent (содержимое ini-файла параметров устройства)
    function loadDevXContent(devs){
        let DevXList = Object.keys(devs);//получаю список объектов 1-го уровня, а именно dev..
        console.warn(DevXList);
        let i = DevXList.length;
        while (i-- !=0) {
            loadFiles(devs[DevXList[i]]);//проход по вложенным файлам для загрузки контента с обработкой
        }
        console.log(devs);
    }
