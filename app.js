// https://code.visualstudio.com/docs/nodejs/nodejs-tutorial
var crc = require('./crc16');
var LnkManager = require('./lnkmanager');
var HTTPServer = require('./http_server');
var w = require('./ws_server');
var AjLoader = require('./ajiniload');

var cnt = 0;
var sps = 0;
var sec = 0;

  AjLoader.getDevices();

  var lmsp = new LnkManager.TSerialPort(
    {port:'COM4',//название порта
        settings:  { // настройки порта
              baudRate: 115200, // this is synced to what was set for the Arduino Code
              dataBits: 8, // this is the default for Arduino serial communication
              parity: 'none', // this is the default for Arduino serial communication
              stopBits: 1, // this is the default for Arduino serial communication
              flowControl: false // this is the default for Arduino serial communication
          }
    }
  );

var lm = new LnkManager.TLnkManager({port:lmsp});
//добавляю слот (запрос ID)
lm.addSlot({name:'ID', cmd:crc.addCRC16toFrame([0x01,0x11])},
        function(data, slot){
          var s = slot.name+':';
          if (slot.state.ErrTimeOut) {
            s+='TimeOut'; 
          }
          else {
            if (crc.crc16(data, data.length) == 0) {
              s += new String(data, "UTF-8");
            }
            else {
              s+='CrcError'; 
            }
          }
          console.log(sec+':'+(cnt++)+':'+s);
          slot.in = s;
        });

lm.addSlot({name:'RAM',cmd:crc.addCRC16toFrame([0x01,0x03,0x00,0x00,0x00,0x7B])},
        function(data, slot){
          console.log(sec+':'+(cnt++)+':'+toHexString(data));
        });

lm.start();//запуск манагера в автономное плавание

 function getID () {
   var slot = lm.getSlotByName('ID');
   if (slot != null) {
      return slot.in;
   }
   else {
     return 'err: not ID slot';
   }
 }

 function sendIDData(req, response){
   var s = getID();
   response.send(s);
 }
 //HTTP сервер
 //вот так вызыыать http://localhost:3000/slot
  var srv = new HTTPServer.THTTPServer(3000);
  srv.addGet('/data', sendIDData);//HTTPServer.getExample);

 //WebSocket
  var wss = w.TWSServer(8081);

  function toHexString(arr) {
    var str ='';
    for(var i = 0; i < arr.length ; i++) {
    str += ((arr[i] < 16) ? "0":"") + arr[i].toString(16);
    }
    return str;
    }

    var i = 0;
    var timerId;
    timerId = setTimeout(procUpdate, 1000);
    function procUpdate(){
      console.log('wake up:'+(i++));
      timerId = setTimeout(procUpdate, 1000);
    }