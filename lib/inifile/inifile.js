module.exports.getSectionListFromIniFile = getSectionListFromIniFile;
module.exports.isSection = isSection;//true - секция существует
//modules
var fs = require('fs');
var iconv = require('iconv-lite');//использую для перекодировки cp1251 в utf8


//функции работы с windows ini-файлами
//loadLinesFromIniFile загрузка содержимого ini-файла
//и преобразование в массив строк
function loadLinesFromIniFile(filename){
    if (filename == undefined) return [];//возвр пустой массив если имя файла не определено
    var text = fs.readFileSync(filename);//загрузил ini а он в cp1251 кракозябах  
    var arr = iconv.encode(iconv.decode(text, "cp1251"), "utf8").toString().split("\n");//перекодировка cp1251 в utf8
    arr.forEach(function(value, index) { arr[index].trim() });
    return arr;
}

function getSectionBegin (section, list){
    var result = undefined;
    for (var i = 0; i < list.length; i++){
        var s = list[i]; 
        if (s.indexOf(section) == 0) {//с 0-индекса должна начинаться строка с секцией [имя секции]
            return result = (++i == list.length)?
                                --i://секция пустая и находится в конце файла то начало будет равно концу
                                  i;//секция НЕ пустая, содержимое данных секции начинается со следующей строки
        }
    }
    return result;
}

function getSectionEnd (begin, list) {
    var result = begin;//если я не найду конец секции то конец будет равен началу
    //раз нашёл вхождение в секцию, то теперь надо найти последнюю строку в ней
    //для чего найти начало другой секции [other] или конец файла (текста, в моём случае)
    //Признаки "другой секции"
    //  1) начинается с начала строки (0-индекс)
    //  2) начинается с символа "["
    for (i = begin; i < list.length; i++){
        s = list[i]; 
        if (s.indexOf('[') == 0) {//с 0-индекса должна начинаться строка с секцией [имя секции]
                if (i > 0) {
                    //проверка на последнюю строку
                    if ((i+1) == list.length) //последняя строка
                        result = begin;
                    else
                        result = --i;
                }
                else
                    result = 0;
                return result;
            }
        }
    return result;
}

//возвращает массив строк из секции
//если строка пустая то удалить.
function getSelectedList(list, begin, end){
    var result = [];
    if (end == begin) return result;
    result = list.slice(begin, end+1);
    result.forEach(function(item, index){
        if (item == "\r") {
            result.splice(index, 1);
        }
    });
    return result;
}

//возвращает массив строк из заданной секции, заданного ini-файла
//и так, есть название секции "section" которую надо прочитать, и имя файла "filename"
function getSectionListFromIniFile(section, filename){
    var res = null;
    console.log(section, filename);
    //можно грузануть файл целиком и там разбираться
    var a = loadLinesFromIniFile(filename);
    //теперь найти там строку с текстом "[section]" - это будет начало секции
    var sname = '['+section+']';
    //теперь ищу секцию, это должна быть одиночная строка
    //TO DO файл по строка считал, возможно из-за того что остался (в конце строки)
    //      символ "\r" то название секции не ищется.. надо проверять
    var firstSectionIndex = getSectionBegin (sname, a);//найти начало секции
    if (firstSectionIndex !=undefined) {
        var lastSectionIndex = getSectionEnd (firstSectionIndex, a);//найти конец секции
        var res = getSelectedList(a, firstSectionIndex, lastSectionIndex);
    }
    console.log('getSectionListFromIniFile:',res);
    return res;
} 

//проверка существования секции
//true - секция есть (false в противном случае)
function isSection(section, filename){
    return (getSectionBegin ('['+section+']', loadLinesFromIniFile(filename)) != undefined)? true : false;
}