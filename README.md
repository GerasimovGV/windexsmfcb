Цели обучающего проекта:
1 настройка среды Visual Studio Code для работы с Node.JS
1.1 Запуск скрипта по кнопке "ПУСК" (зелёный треугольник)
1.2 Возможность поставить брейк-поинт
1.3 Пошаговая отладка скриптов в VSC
1.4 Вывод console.log Node в консоль VSC
2 Общение с внешним устройством по последовательному порту
2.1 открытие порта c заданными параметрами связи (bps, чётность, кол-во бит данных и т.п., тайм-аут)
2.2 запись в порт команды 0x03 Modbus RTU и наблюдение реакции внешнего устройства (должен мигнуть светодиод)
2.3 получение от устройства ответа: преобразование его в строку и выдача в console
2.4 организация циклов запрос/ответ для выжимания максимальной производительности (частоты)

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact