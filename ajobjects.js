module.exports.createObjectsList = createObjectsList;
module.exports.createSignalShort = createSignalShort;
module.exports.getScaleNameAndMath = getScaleNameAndMath;

const
  V_object_name = 0; // имя объекта
  V_name        = 1; // имя переменной
  V_comment     = 2; // пояcнение назначения переменной
  V_const_HEX   = 3; // базовое значение переменной в HEX
  V_const_Phys  = 4; // базовое значение переменной в физических значениях
  V_mem_HEX     = 5; // текущее значение переменной в HEX
  V_mem_Phys    = 6; // текущее значение переменной в физических значениях
  V_meas_unit   = 7; // единицы измерения
  V_PN          = 8; // имя параметра

//передаю текс ini-файла и секцию которую трэба перпеварить в список объектов
function createObjectsList(text, section){
    
}

var TDeviceList = function (){
  this.vars  = [];//секция var
  //this.value = [];//параметр из RAM / FLASH / CD
  this.values = {};//тут появятся объекты с ключами RAM / FLASH / CD
                   //к которым добавятся массивы параметров 
                   //и у каждого ключа, есть объект req с описанием запроса

}
//оказывается после объекта надо ставить экспорт класса
module.exports.TDeviceList=TDeviceList;

//выявляет минимальный и максимальный адрес регистра
//на выходе объект:
//  *мин-адрес
//  *макс-адрес
//  *кол-во регистров (если последний рег-р 4-х байтный то + 1 рег)
//  *карту привязок: регN>[парX, парY ...]
//Зная с како-го по какой рег-р я читаю, становится возможным
//раскидать полученный массив регистров по параметрам
//Или:
//  дать каждому Параметру номер первого байта
//  в принятом массиве данных (как сейчас в Aj)? 
//memType: ram/flash/cd
TDeviceList.prototype.createRequest = function (memType) {
  if (!(memType in this.values)) //если нет массива с данными секции
    return null; //то на выход с возвратом null
  //собрал все регистры
  var a=[];
  var i = this.values[memType].length;
  this.values[memType].forEach(function(o){
    var r = o.regNum;
    a.push({r, o});
  });
  //отсортировал их по возрастанию адресов
  //(c наибольшим индексом - имеет наибольший адрес)
  a.sort(function (a, b) {
    if (a.r < b.r) { return -1;}
    if (a.r > b.r) { return  1;}
    return 0; //a = b
  });
  var rMin = a[0].r;//начальный регистр
  var rMax = a[a.length-1].r;//конечный регистр в пакете
  //если регистр 
  //тут тернарный оператор. Для 1-байт Параметров, число будет отрицательным
  //значит надо сделать результат НОЛЬ
  var o = a[a.length-1].o;
  var hRegSize = Math.round((o.bytes / 2) - 1);//последний регистр может быть 4-х байтным
  var regs = rMax - rMin;
    if (regs == 0) regs = 1;//не может быть ноль регистров в запросе
    regs +=hRegSize;//кол-во регистров в запросе (с учётом размера последнего регистра)
  //формирование объекта реквеста
  var req = {
    rmin : rMin, //начальный адрес
    rcount : regs,//кол-во регистров
    rmax : rMax //максимальный адрес
  };
  console.log(req);
  this.values[memType].req = req;//добавляю объект реквеста в массив секции
}

//Добавление данных во все объекты списка
//memType: ram/flash/cd
TDeviceList.prototype.addData = function (memType) {
  var i = this.values[memType].length;
  while (i-- !=0){
    let o = this.values[memType][i];
    o.addData(0);
  }  
}

TDeviceList.prototype.setVarFromIniStr = function (varStr) {
  console.log('setVar',varStr);
  var i = varStr.indexOf('=');
  if (~i) {//есть символ = в строке
    var key = varStr.slice(0,i);
    var value = varStr.slice(i+1);
    this.vars.push({key, value});
    console.log(this.vars);
  }
}

/** @description Из списка строк создаёт значения шкал
 * 
 * @param {Array} list - массив строк с масштабными коэффициентами из ini-файла
 */
TDeviceList.prototype.setVarsFromList = function (list) {
  console.log('setVarsFromList',list);
  list.forEach(function(item, index){
    console.log(item);
    this.setVarFromIniStr(item);
  }, this);
}


//Создаёт объект-параметр из ini-строки и добавляет его в список
//IniStr - строка параметра прям из ini-файла
//memType - в какой раздел (RAM/FLASH/CD e.t.c) его добавлять
//          по умолчанию (если memType не задан) складывать в RAM
TDeviceList.prototype.setValueFromIniStr = function (IniStr, memType) {
  var o = createSignalShort(IniStr, this);
  if (o != undefined) {//если объект создался, то пытаюсь всунуть его в массив
    if (!(memType in this.values))//проверю есть ли у меня массив и именем как предан в memType (RAM/FLASH/CD e.t.c)
      this.values[memType]=[]; //если нет, то создаю его
    this.values[memType].push(o);//и добавляю в него созданный объект
  }
}

/** @description Из списка строк создаёт объекты параметров
 * 
 * @param {Array}  list - массив строк с масштабными коэффициентами из ini-файла
 * @param {String} memType  - в какой раздел (RAM/FLASH/CD e.t.c) его добавлять
 *                            по умолчанию (если memType не задан) складывать в RAM
 */
TDeviceList.prototype.setValuesFromList = function (list, memType) {
  console.log('setValuesFromList ',list);
  list.forEach(function(item, index){
    console.log(item);
    this.setValueFromIniStr(item, memType);
  }, this);
}

//задаю Key, и массив, получаю value=array[key] или null
function getArrValueByKey(key, arr) {
  var i = arr.length;
  while (i-- != 0) {
      if (arr[i].key == key){
          return arr[i].value;
      }
  }
  return null;
}

//Парсит списочную шкалу
//x00#300A|75mV# 0,0984375/x01#500A|75mV# 0,1640625/x02#750A|75mV# 0,24609375/x01/
function getValueFromListsVar(list){
  var a = list.split(/[/]/);//получил массив
  if (a.length !=0) {
    a.splice(a.length-1,1);//удалил из него "ложный" элемент
    var key = a[a.length-1];//последний элемент это номер шкалы который требуется выбрать
    var i = a.length - 1;//
    while (i-- !=0) {
      var b = a[i].split(/[#]/);//получил массив
      if (b[0] == key) {
        return b[b.length-1];//в последнем элементе находится шкала
                             //почему в последнем? потому что в среднем
                             //может находится а может не находится комментарий
      }
    }
  }
  console.log('getValueFromListsVar:','элемент не найден');
  return 1;//значение по умолчанию
}

//Возвращает значение переменной из VARS[]
TDeviceList.prototype.getVarValue = function (name) {
  var o = getScaleNameAndMath(name);
  //в o.VarName содержится ключ, надо найти его Value в массиве VARS
  var value = getArrValueByKey(o.VarName, this.vars);
  //теперь конвертируется ли во float, если да то это "нормальный" коэффициент
  //если нет, то проверить, возможно он списочный
  if (isNaN(parseFloat(value.replace(",",".")))){//не сконвертилось, пробовать списочный параметра
    //он в таком виде x00#300A|75mV# 0,0984375/x01#500A|75mV# 0,1640625/x02#750A|75mV# 0,24609375/x01/
    value = getValueFromListsVar(value);
  }
  //получил значение, теперь проверяю мат операции, если есть применяю
  value = Number(parseFloat(value.replace(",",".")));
  switch (o.MathOpCode) {
    case '*': value = value * o.MathValue;
              break;
    case '%': if (o.MathValue != 0) {
                value = value / o.MathValue;
              }
              else {
                value = 1.0;
              } 
              break;
  }
  console.log(o.VarName,'=',value);
  return value;
}

//короче, шкала может быть:
//такой IsScale*1,8 - умножение
//или такой IsScale%1,8 - деление (% заменяет слэш)
//или IrScale=x00#300A|75mV# 0,0984375/x01#500A|75mV# 0,1640625/x02#750A|75mV# 0,24609375/x01/
//И надо выделить имя переменной и математические операции и коэффициент
function getScaleNameAndMath(name){
  var MathOpCode = '';//знаки *         или %
  var MathValue  = '';//      множитель или делитель
  var VarName    = name;//имя переменной вернётся неизменным если парсить будет нечего
  var a = name.split(/[*%]/);//получил массив
  if (a.length >=1) {VarName = a[0];}//получил имя переменной 
  if (a.length > 1) {//нашёлся разделитель и в массиве более 1й строки
    var i = name.search(/[*%]/);//место расположения мат-оператора (если есть)
    MathOpCode = name.slice(i,i+1);//выделил мат оператор
    MathValue = Number(a[1].replace(",","."));//заменить "," на "." для дальнейших вычислений
  }
  //      IsScale  * или %     2.45
  return {VarName, MathOpCode, MathValue};
}

//Возвращает ссылку на объект по его имени из REGS[] ram/flash/cd
TDeviceList.prototype.getByName = function (name) {
  
}

var TSignal = function (owner) {
  this.owner = owner;
  this.rawData = 0;//данные объекта
  this.pn='pxxxxx';//номер параметра в списке 'p00230='
  this.name = '';//имя тега, напр-р Iexc
  this.comment = '';//краткое описание тега "ток возбуждения"
  this.msu='';//единицы измерения "А"
  this.scale=1.0;//коэффициент для перевода из hex в физическую величину
  this.scaleStr='1';//строка со шкалой из ini
  this.base = 0;//уставка
  this.option = 0;//опции параметра (напр, для TFloat)
  this.objName = 'TSignal';//тип объекта сигнала
  this.regNum = 0;//номер начального регистра rXXXX
  this.regOffs = false; // true  - rXXXX.H - старший байт для TByte параметров 
                        // false - rXXXX.L - младший байт
  this.regBitNum = 1;//номер бита для TBit-параметров
  this.regStr = 'r000A.1';//строка регистра
  this.bytes = 2;//кол-во байт занимаемых Параметром
  this.isMaster = false;//true - Мастер-параметр, от него зависят другие параметры
  this.depend = '';//имя параметра от которого зависит это параметр
  this.IC = 1;//коэффициент зависимости от Мастера
  this.notAddressable = false;//true - параметр требуется для расчётов и не входит в адресное пространство устройства
}

//Добавление данных
TSignal.prototype.addData = function (data) {
  this.rawData = data;
}

//Формирует строку для 
TSignal.prototype.getIniString = function (data) {
  //
}



//WORD
function TWORD (owner) {
	//вызываю конструктор родителя
	TSignal.call(this,owner);
	//данные потомка
}

//Создал объект TWORD.prototype, который наследуется от TSignal.prototype
TWORD.prototype = Object.create(TSignal.prototype);
// Устанавливаем свойство "constructor" для ссылки на класс TWORD
TWORD.prototype.constructor = TWORD;
//Заменяю метод addData
TWORD.prototype.addData = function(data){
  this.rawData = data;
};

//function getParamNumber(source:string):string;
//function getScaleCodes(Name:string; var MathOpCode, MathNumber:string ):string;//return Scale Name
//procedure CreateParameterByName(Name:string;PList:TStringList;buffer:pointer;vl:PViewList);

//  s   [0]    [1]       [2]     [3]  [4]      [5]       [6]     [7]
//WORD=name/comments/objecttype/addr/mbreg/measure unit/scale/bytesize/
//advanced
//  s   [0]    [1]       [2]     [3]  [4]      [5]       [6]     [7]      [8]         [9]   [10]
//WORD=name/comments/objecttype/addr/mbreg/measure unit/scale/bytesize/IndepValueName/IC/BaseConst/
function create_word (pN, prop, owner){
  var o = new TWORD (owner);
  var result = null;
  console.log('create_word');
  o.pn = pN;//pXXXXX
  o.objName = prop[2];//TWORD
  o.bytes = 2;//кол-во байт занимаемых параметром
  //1) в массиве должно быть не менее 7 строк
  if (prop.length >=7) {//имеет смысл расматривать
    //2) параметр входит в адресное пр-во контроллера? или он вирутальный
    if (prop[4]=='') o.notAddressable = true;//виртуальный параметр
    //3) привязка к адресам Модбас
    o.regStr = prop[4];//строка с адремов регистра
    o.regNum = parseInt(o.regStr.slice(1,5),16);//из r002E->002E->46 b.ModBusReg:=StrToInt('$'+copy(s[4],2,4));
    //4) описание
    o.name = prop[0];// напр Iexc
    o.comment = prop[1];//напр. ток возбуждения
    o.msu = prop[5];//единицы измерения
    //5) Шкала
    o.scaleStr = prop[6];//строка шкалы
    o.scale = getVar(o.scaleStr, owner);//получить значение шкалы
    //6)Зависимости
    if (prop.length == 11) {
      o.depend = prop[8];//IndepValueName
      if ((o.depend == '@') && (prop[9] == '')){
        o.isMaster = true;
      }
      else {
        let f = parseFloat(prop[9].replace(",","."));//замена "," на "." если есть
        o.IC = (~isNaN(f))? f : 0;
      }
    }
    //7) базовое значение (для уставок)
    o.base = prop[prop.length - 1];//уставка - это последнее значение
    //Результат
    result = o;
  }
  return result;
}

//если шкала не число то искать в списке VAR[]
//если число то вернуть то что передавал
//если всё с ошибками то вернуть Number(1.0)
function getVar(name, owner) {
  var result = 1.0;
  var f = parseFloat(name.replace(",","."));//замена "," на "." если есть
  if (!isNaN(f)) {//это простое число типа 0.001
    result = f;
  }
  else {//запрос к владельцу, может он знает что это за коэффициент
    if (owner != null) {
      result = owner.getVarValue(name);
    }
  }
  return result;
}

function createSignalExt(iniStr, options, owner) {
  console.log('createSignalExt',iniStr);
  //1) отделю Pn от параметра и его свойств. Их разделяет "="
  //   найти 1-й знак "="
  var pn = '';//номер параметра 'p04600'
  var prop = '';//строка свойств 'Qoe/Реакт.мощность отн.единицы/TFloat/x005C/r002E/oe/1/4/0/0/'
  var s = '';
  var i = iniStr.indexOf('=');
  var o = null;//объект параметра который должне создаться по итогам
  if (~i) {//есть символ = в строке
    pn = iniStr.slice(0,i);
    //2) выделяю строку свойств (то что после "=")
    prop = iniStr.slice(i+1);
    //3) из пропов делаю массив строк разделённых до того символом "/"
    s = prop.split('/');//получил массив
    if (s.length !=0) { s.splice(s.length-1,1);};//удалил из него "ложный" элемент	
    //4) Создать объект
    o = create_object(pn,s[2],s, owner);
  }
  return o;
}

/*
function create_object_short(s:string;buffer:pointer;l:PViewList):PSignal;
//s      - p..=name/comment/objtype/../..../BaseValue;
//buffer - pointer to mem buffer
//l      - list of items
*/
function createSignalShort(iniStr, owner) {
  var o = createSignalExt(iniStr, '0/0/0', owner);
  console.log('createSignalShort',o);
  return o;
}

function create_object(pN,SignalType,prop, owner) {//r;vl:PViewList;var s:TStringList;o:string):PSignal;
  //if ObjectName='TBit'         then create_bit          (PN,buffer,vl,s,o);
  //if ObjectName='TByte'        then create_byte         (PN,buffer,vl,s,o);
  if (SignalType=='TWORD') return create_word (pN, prop, owner);//buffer,vl,s,o)};
  //if ObjectName='TShortInt'    then create_ShortInt     (PN,buffer,vl,s,o);
  //if ObjectName='TInputFilter' then create_InptFltr     (PN,buffer,vl,s,o);
  //if ObjectName='TDWORD'       then create_dword        (PN,buffer,vl,s,o);
  //if ObjectName='TInteger'     then create_Integer      (PN,buffer,vl,s,o);
  //if ObjectName='TLongint'     then create_Longint      (PN,buffer,vl,s,o);
  //if ObjectName='TIntegral'    then create_Integral     (PN,buffer,vl,s,o);
  //if ObjectName='TFrequency'   then create_Frequency    (PN,buffer,vl,s,o);
  //if ObjectName='TSoftTimer'   then create_SoftTimer    (PN,buffer,vl,s,o);
  //if ObjectName='TDiff'        then create_diff         (PN,buffer,vl,s,o);
  //if ObjectName='TPrmList'     then create_PrmList      (PN,buffer,vl,s,o);
  //if ObjectName='TSQRWord'     then create_sqrword      (PN,buffer,vl,s,o);
  //if ObjectName='TIPAddr'      then create_ipaddr       (PN,buffer,vl,s,o);
  //if ObjectName='TTimeCnt'     then create_TimeCnt      (PN,buffer,vl,s,o);
  //if ObjectName='TFloat'       then create_Float        (PN,buffer,vl,s,o);
}
